package Shapes;

public class Square extends Quadrilateral{
    public Square(double side) {
        super(side,side);
    }

    @Override
    public void setLength(double length) {
        this.length = length;

    }

    @Override
    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public double getPerimeter() {
        return (length *4);
    }

    @Override
    public double getArea() {
        return Math.pow(width,2);
    }
//    public Square (double sides){
//        super(sides, sides);
//    }
//
//    @Override
//    public double getPerimeter() {
//        return 4 * width;
//    }
//
//    @Override
//    public double getArea() {
//        return width * width;
//    }
//
//    @Override
//    public double getArea() {
//        return Math.pow(this.length,2);
//    }
//    @Override
//    public double getPerimeter() {
//        return (this.length * 4);
//    }

}

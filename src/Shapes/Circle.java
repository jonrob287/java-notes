package Shapes;

public class Circle {
    //create instance variable objects
    private double radius;
    private String color;

    Circle() {
        radius = 1.0;
        color = "red";
    }
    public Circle(double r, String color) {
        this.radius = r;
        this.color = color;
    }
    public double getRadius() {
        return radius;
    }
    public double getArea() {
        return radius*radius*Math.PI;
    }
    public String getColor() {
        return color;
    }
}



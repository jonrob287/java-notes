package Shapes;

public class ShapesTest {
    public static void getPerimeter(Rectangle e){
        System.out.println(e.getPerimeter());
    }
    public static void getArea(Rectangle e){
        System.out.println(e.getArea());
    }


    public static void main(String[] args) {
        Measurable myShape = new Square(7);
        System.out.println(myShape.getArea());
        System.out.println(myShape.getPerimeter());

        Measurable myShape2 = new Rectangle(25,14);
        System.out.println(myShape2.getArea());
        System.out.println(myShape2.getPerimeter());
//        Rectangle box1 = new Rectangle(5,4);
//        Rectangle box2 = new Square(5);
//        Square box3 = new Square(7);
//        getPerimeter(box1);
//        getArea(box1);
//        getPerimeter(box2);
//        getArea(box2);
//        System.out.println(box3.getPerimeter());
//        System.out.println(box3.getArea());


    }

}

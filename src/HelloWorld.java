import java.util.Scanner;

public class HelloWorld {
    //psvm
    public static void main(String[] args) {
        //sout
//        System.out.print("Hello World!");
//        System.out.println("Code in one line");
//        System.out.println(" ");
//        System.out.println("hello World!");
//        System.out.println("Code in two lines");

//        DATA TYPES
        //8 PRIMITIVE DATA TYPES
        byte age = 12;
        short myShort = -32768;
        int myInt = 120;
        long myLong = 1228888888989064257L;
        float myFloat = 123.456F;
        double myDouble = 123.456;
        char myChar = 'A';
        boolean myBoolean = true;

//        int x = 10;
//        int y =25;
//        System.out.println(x + y);
//        System.out.println("x plus y = " +(x + y));

//        byte myByte2 = -129; //out of range
//        byte myByte3 = -128;
//        byte myByte4 = 128; //out of rang
//        byte myByte5 = 127;
        //Exercise
//        int favoriteNum = 287;
//        System.out.println(favoriteNum);

//        String myName = "Jonathan";
//        System.out.println(myName);
//
//        char myName2 = "Jonathan"; //   java.lang.String can not be converted to char
//        System.out.println(myName2);

//        String myName = 3.14159;    // double can not be converted to java.lang.String
//        System.out.println(myName);

//        long myNum; //illegal start of expression
//        System.out.println(myName);

//        long myNum = 3.14; //incomplete types possible lossy conversion from double to long
//        System.out.println(myName);

//        long myNum = 123L;
//        System.out.println(myName); //prints out 123

//        long myNum = 123;
//        System.out.println(myName); //prints out 123

//        Why does assigning the value 3.14 to a variable declared as a long not compile, but assigning an integer value does?
//        3.14 is considered a double

//        float myNum = 3.14; // possible lossy conversion from double to float
//        System.out.println(myNum);
//        What happens? What are two ways we could fix this?
//        double myNum = 3.14;
//        System.out.println(myNum);
//        float myNum = 3.14F;
//        System.out.println(myNum);
//
//        int x = 10;
//        System.out.println(x++);    //prints out 10
//        System.out.println(x);      //prints out 11
//        int x = 10;
//        System.out.println(++x);    //prints out 11
//        System.out.println(x);      //prints out 11
//        What is the difference between the above code blocks? Explain why the code outputs what it does.
//        the first code x++ prints out the x variable but adds 1 to the next time x is called
//        the second code ++x prints out plus 1 to the x variable but does not add 1 when x is called again

//        double class //class is recognized and will not be allowed to assign 'class' as a variable

//        String theNumberEight ="eight"; //illegal character
//        Object o = theNumberEight;
//        int eight = (int) o;//not a statement
//        System.out.println(eight);

//        int eight = (int) “eight”;  //illegal character //not a statement

//        What are the two different types of errors we are observing?
//        illegal character and not a statement
//        does not allow to change string to inti


//        int x =5;
////        x = x + 6;
//        x += 6; // shorthand operator
//        System.out.println(x);

//        int x = 7;
//        int y = 8;
////        y = y * x;
//        y *= x;   //shorthand operator
//        System.out.println(y);

//        int x = 20;
//        int y = 2;
//        x = x / y;
//        y = y - x;
//        x /= y; //shorthand
//        y -= x; //shorthand
//        System.out.println(x);
//        System.out.println(y);

//        shorthand assignment operator
/*
            ++
            --
            +=
            -=
            *=
            /=
 */
//        What happens if you assign a value to a numerical variable that is larger (or smaller) than the type can hold? What happens if you increment a numeric variable past the type's capacity?
//        Hint: Integer.MAX_VALUE is a class constant (we'll learn more about these later) that holds the maximum value for the int type. (edited)
//
//        int z = Integer.MAX_VALUE + 1;
//        System.out.println(z);

//        String name = "Bravo";
////        System.out.println(name);
////        System.out.format("Hello there, %s. Nice to see you!", name);
//
//        String greet = "Hola";
//        System.out.format("%s, %s!\n", greet, name);
//
//        System.out.println(greet + ", " + name + "!");

//        SCANNER CLASS - GET INPUT FROM THE CONSOLE
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter something: ");    //Prompting the user to enter data
        String userInput = scanner.next();          //Obtaining the value the user input
        System.out.println(userInput);              // souting the userInput value
















    }// END OF MAIN
} //END OF CLASS

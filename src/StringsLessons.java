import jdk.swing.interop.SwingInterOpUtils;

public class StringsLessons {
    public static void main(String[] args) {
        // Strings
//        String message = "Hello World!";
//        System.out.println(message);
//
//        String anotherMessage;
//        anotherMessage = "Another Message Assigned!";
//        System.out.println(anotherMessage);
//
////      CONCATENATION
//        String myName = "Hello World, " + "my name is Stephen.";
//        System.out.println(myName);
//
//        System.out.println(message + " " + anotherMessage + " " + myName);

//        Dont Do
//        if (message= "hello")

//              Do This
//        String message = "Hello World!";
//        if(message.equals("Hello World!")){
//            System.out.println("Message is Correct!");
//        }

//        STRING COMPARISON METHODS
//        .equals(), .equalsIgnoreCase(), .startsWith(), .endsWith()
        String input = "Bravo Rocks!";
        System.out.println(input.equals("Bravo Rocks!")); // TRUE
        System.out.println(input.equals("Bravo Rocks"));  // FALSE
        System.out.println(input.equals("bravo rocks!")); // FALSE
        System.out.println(input.equals("Bravo Rocks! "));// FALSE
        System.out.println(input.equals("Bravo Rocks !"));// FALSE

        System.out.println(input.equalsIgnoreCase("BRAVO ROCKS!")); // TRUE
        System.out.println(input.equalsIgnoreCase("Bravo Rock!"));  // False

        System.out.println(input.startsWith("Bravo"));  //True
        System.out.println(input.startsWith("bravo"));  //FALSE

        System.out.println(input.endsWith("rocks!"));   //FALSE
        System.out.println(input.endsWith("!"));        //TRUE
        System.out.println(input.endsWith("Rocks!"));   //TRUE
        System.out.println(input.length());




    }
}

package Collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

class Student{
    int roll;
    String name;
}

public class CollectionsLecture {

    /**
     * .add()
     * .remove()
     * .get()
     * .indexOf()
     * .contain()
     * .lastIndexOf()
     * .
     *
     *
     * */





//Examples
    public static void main(String[] args) {
        ArrayList<String> list1 = new ArrayList<String>();// declared array list and defined (new) given data type

        list1.add("James");
        list1.add("Jonathan");
        list1.add("Jill");
        list1.add("Jim");
        list1.remove("Jim");
        list1.remove(2);
        list1.add("Jamie");
        System.out.println("list of list1 is : " + list1);
//
//        Student s1= new Student();
//        s1.roll = 294859;
//        s1.name = "June";
//
//        ArrayList list2 = new ArrayList();
//
//        list2.add("Jim");
//        list2.add(100);
//        list2.add(s1);
//        System.out.println("list 2 contains: " + list2);
//
//        ArrayList<Integer>numbers1 = new ArrayList<Integer>();
//        numbers1.add(4);//adding to the ArrayList element
//        numbers1.add(1);//adding to index 3
//        numbers1.add(2);
//        numbers1.add(3);
//        numbers1.add(5);
//        numbers1.add(6);
//        numbers1.add(15);
////        numbers1.addAll(list2);
//        System.out.println("list of numbers in list : " + numbers1);
//        System.out.println(numbers1.get(0)); //getting element at index 0
//        numbers1.set(1,8);//setting element 8 to index 1
//        System.out.println(numbers1);
//        numbers1.remove(0);//removing element at index 0
//        System.out.println(numbers1);
//        Collections.sort(numbers1);//sorts the array from least to greatest
//        System.out.println(numbers1);
//        System.out.println(numbers1.size());

        ArrayList<String> roasts = new ArrayList<String>();
        roasts.add("Medium");
        roasts.add("Rare");
        roasts.add("Medium-Well");
        roasts.add("Well-Done");
        System.out.println(roasts);
        System.out.println(roasts.contains("Well-Done")); // can be used as condition if roasts.containts() do
        System.out.println(roasts.contains("True")); // checking to see if true is contained in array
        System.out.println(roasts.lastIndexOf("Medium"));// returns the index of given value
        System.out.println(roasts.lastIndexOf("Rare"));
        System.out.println(roasts.isEmpty());//checks to see if array is empty true or false
        roasts.clear();//clears the array
        System.out.println(roasts);
        System.out.println(roasts.isEmpty());

    }


}

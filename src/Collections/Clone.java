package Collections;

import java.util.ArrayList;

public class Clone {

    public static void main(String[] args) {
        ArrayList<String> al = new ArrayList<>();

        //adding elements to arrayList
        al.add("Apple");
        al.add("Banana");
        al.add("Pear");
        al.add("Mango");
        al.add("Grapes");
        System.out.println("ArrayList is: " + al );

        ArrayList<String> al2 = (ArrayList<String>)al.clone();
        System.out.println("Shallow copy of ArrayList: " + al2);

        // add and remove on cloned arrayList
        al2.add("Fig");// adding fig
        al2.remove("Grapes"); // removing grapes
        System.out.println(al); //printing out al ArrayList after adding and removing
        System.out.println(al2); // printing the clone ArrayList after adding and removing


    }
}

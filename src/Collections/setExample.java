package Collections;

import java.util.ArrayList;

public class setExample {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(3);
        arrayList.add(5);
        arrayList.add(7);
        arrayList.add(6);
        arrayList.add(8);
        arrayList.add(9);
        arrayList.add(2);
        arrayList.add(1);
        System.out.println("ArrayList before update: " + arrayList);

        arrayList.set(0, 11);//updating first element
        arrayList.set(1,8);// updating second element
        System.out.println("ArrayList after update:  " + arrayList);



    }
}

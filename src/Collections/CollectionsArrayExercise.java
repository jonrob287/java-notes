package Collections;

import java.awt.image.AreaAveragingScaleFilter;
import java.util.*;

public class CollectionsArrayExercise {
    public static void main(String[] args) {
        /**TODO:
            Create a program that will print out an array of integers.
            Create a program that will print out an array of strings
            Create a program that iterates through all elements in an array. The array should hold the names of everyone in Alpha Class
            Create a program to insert elements into the array list. This element should be added on the first element and last. You can use a previous array or create a new one.
            Create a program to remove the third element from an array list. You can create a new array or use a previous one.
            Create an array of Dog breeds. Create a program that will sort the array list.
            Create an array of cat breeds. Create a program that will search an element in the array List.
            Now create a program that will reverse the elements in the array.
 */
//        Create a program that will print out an array of integers.
        ArrayList<Integer> numbersArray = new ArrayList<>();
        numbersArray.add(1);
        numbersArray.add(2);
        numbersArray.add(3);
        numbersArray.add(4);
        numbersArray.add(5);
        System.out.println("ArrayList of Integers: " + numbersArray);

//        Create a program that will print out an array of strings
        ArrayList<String> stringsArray = new ArrayList<>();
        stringsArray.add("One");
        stringsArray.add("Two");
        stringsArray.add("Three");
        stringsArray.add("Four");
        stringsArray.add("Five");
        System.out.println("ArrayList of Strings: " + stringsArray);

//        Create a program that iterates through all elements in an array. The array should hold the names of everyone in Alpha Class
        ArrayList<String> bravoStudentsArray = new ArrayList<>();
        bravoStudentsArray.add("Adrian");
        bravoStudentsArray.add("Jonathan");
        bravoStudentsArray.add("MaryAnn");
        bravoStudentsArray.add("Sandra");
        bravoStudentsArray.add("Eric");
        bravoStudentsArray.add("Henry");
        Iterator<String>iterator = bravoStudentsArray.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

//        Create a program to insert elements into the array list. This element should be added on the first element and last. You can use a previous array or create a new one.
//        .addLast
//        .addFirst
        ArrayList<String> animalsArray = new ArrayList<>(Arrays.asList(
                "Monkey",
                "Giraffe",
                "Elephant",
                "Lemur",
                "Peacock"
        ));
        animalsArray.add(0,"Ferret");
        System.out.println(animalsArray);
        animalsArray.add(6,"Parrot");
        System.out.println(animalsArray);


//        Alternative solution seems like it would be better in a practical sense
        LinkedList<String> names = new LinkedList<String>();

        //Adding elements to the Linked list
        names.add("Steve");
        names.add("Carl");
        names.add("Raj");

        //Adding an element to the first position
        names.addFirst("Negan");

        //Adding an element to the last position
        names.addLast("Rick");
        System.out.println(names);

//        Create a program to remove the third element from an array list. You can create a new array or use a previous one.
        numbersArray.remove(2);
        System.out.println(numbersArray);

//        Create an array of Dog breeds. Create a program that will sort the array list.
        ArrayList<String> dogListArray = new ArrayList<>();
        dogListArray.add("Poodle");
        dogListArray.add("Boxer");
        dogListArray.add("Pit-bull");
        dogListArray.add("Weenie Dog");
        dogListArray.add("German Shepard");
        Collections.sort(dogListArray);
        System.out.println(dogListArray);

//        Create an array of cat breeds. Create a program that will search an element in the array List.
        ArrayList<String> catListArray = new ArrayList<>();
        catListArray.add("Bengal");
        catListArray.add("Siamese");
        catListArray.add("Sphynx");
        catListArray.add("Persian");
        if(catListArray.contains("Bengal")){
            System.out.println("I am a Cat who is a Bengal!");
        }else{
            System.out.println("No Bengal");
        }
//        .contains


//        Now create a program that will reverse the elements in the array.
        System.out.println(catListArray);
        Collections.reverse(catListArray);
        System.out.println(catListArray);
    }
}





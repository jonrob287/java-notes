package Collections;

import org.w3c.dom.ls.LSOutput;

import java.util.HashMap;
import java.util.Set;

public class CollectionsHashMapExercise {
//    Collections
    /*TODO:
            Create a new class called CollectionsHashMapExercise
            Create a program that will append a specified element to the end of a hash map.
            Create a program that will iterate through all the elements in a hash map
            Create a program to test if a hash map is empty or not.
            Create a program to get the value of a specified key in a map
            Create a program to test if a map contains a mapping for the specified key.
            Create a program to test if a map contains a mapping for the specified value.
            Create a program to get a set view of the keys in a map
            Create a program to get a collection view of the VALUES contained in a map.
    */
    public static void main(String[] args) {

        HashMap<String, String> country = new HashMap<>();
        country.put("England", "London");
        country.put("Ireland", "Dublin");
        country.put("Italy", "Rome");
        country.put("Germany", "Munich");
        country.put("United States", "San Antonio");

//        Create a program that will append a specified element to the end of a hash map.
        System.out.println(country);
        country.put("China", "Beijing");
        System.out.println(country);

//        Create a program that will iterate through all the elements in a hash map
        for(String i : country.keySet()){
            System.out.println("Key : " + i + "\nValue: " + country.get(i));
        }

//        Create a program to test if a hash map is empty or not.
//        boolean result = country.isEmpty();
//        System.out.println("Is my HashMap empty? " + result);
//        country.clear();
//        result = country.isEmpty();
//        System.out.println("Is my HashMap empty now? " + result);

//        Create a program to get the value of a specified key in a map
        System.out.println(country.get("United States"));

//        Create a program to test if a map contains a mapping for the specified key.
        HashMap<String, Integer> people = new HashMap<>();
        people.put("Jim",20);
        people.put("Jack",90);
        people.put("Jill",45);
        people.put("John",53);
        people.put("Jillian",12);
        people.put("Joann",55);
        people.put("Julie",18);

        System.out.println(people.remove("John")); //remove john
        if(people.containsKey("John")){
            System.out.println("John is here as a key");
        }else {
            System.out.println("No Johnny B good here!");
        }

//        Create a program to test if a map contains a mapping for the specified value.
        if (people.containsValue(20)) {
            System.out.println("You have a value of 20 ");
        }else{
            System.out.println("No value of 20 here");
        }


//        Create a program to get a set view of the keys in a map
        Set keySet = people.keySet();
        System.out.println("Key set values are: " + keySet);


//        Create a program to get a collection view of the VALUES contained in a map.
        System.out.println("Collection view of values: " + people.values());

    }
}

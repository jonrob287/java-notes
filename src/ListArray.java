//Converting ArrayList to an Array

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListArray {

    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("a");// adding to ArrayList called list
        list.add("b");
        String[] array = new String[list.size()]; // declaring String array defining new array and taking in ArrayList size
        list.toArray(array); // take ArrayList called list and make it an array and then take in array
        System.out.println("Arraylist to Array is: " + Arrays.toString(array));

    }




}

package Movies;

public class Movie {
    private String name;
    private String category;

    public Movie(String name, String category) {
        this.name = name;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    public static String allMovie(){
        Movie[] allMovies = MoviesArray.findAll();
        String allMovie = "";
        for (Movie m : allMovies){
            allMovie += m.getName() + "----" +m.getCategory() +"\n";
        }return allMovie;
    }
    public static String musicMovie(){
        Movie[] musicMovies = MoviesArray.findAll();
        String musicMovie = "";
        for (Movie m : musicMovies){
//            if(m.getCategory())
            musicMovie += m.getName() + "----" +m.getCategory() +"\n";
        }return musicMovie;
    }
}

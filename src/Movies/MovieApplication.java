package Movies;
import java.util.Arrays;
import java.util.Scanner;

import static Movies.Movie.*;
import static Movies.MoviesArray.findAll;


public class MovieApplication {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String choice = "y";
        do {
            System.out.println("What would you like to see?\n" +
                    "0 - exit\n" +
                    "1 - View all Movies\n" +
                    "2 - Comedy\n" +
                    "3 - Drama\n" +
                    "4 - Animated\n" +
                    "5 - Horror\n" +
                    "6 - Sci-fi\n" +
                    "Enter your selection: ");
            int userInput = sc.nextInt();
            if(userInput == 1){
                System.out.println("you entered " +  userInput + "\nAll movies\n============\n");
                System.out.println(Movie.allMovie());
                System.out.println(Arrays.toString(findAll()));
//            }else if(userInput == 2){
//                setCategory("comedy");
//                System.out.println(getCategory() + " " + getName());
//            }else if (userInput == 3){
//                setCategory("drama");
//                System.out.println(getCategory() + getName());
//            }else if (userInput == 4){
//                setCategory("animated");
//                System.out.println(getCategory() + getName());
//            }else if (userInput == 5){
//                setCategory("horror");
//                System.out.println(getCategory() + getName());
//            }else if(userInput == 6){
//                setCategory("scifi");
//                System.out.println(getCategory() + getName());
            }else if(userInput == 0){
                System.out.println("goodbye");
                break;
            }
        } while (true);
    }
}

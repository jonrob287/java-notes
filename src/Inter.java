interface Inter {//interface 1
    public void display1();
}
//second interface
interface Example2{
    public void display2();
}
// this interface is extending both the above interfaces
interface Example3 extends Inter, Example2{

}
class Example4 implements Example3{
    @Override
    public void display1() {
        System.out.println("Display 2 method");
    }

    @Override
    public void display2() {
        System.out.println("Display 3 method");
    }
}
class Demo{
    public static void main(String[] args) {
        Example4 obj = new Example4();
        obj.display1();
    }
}

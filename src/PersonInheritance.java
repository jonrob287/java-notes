public class PersonInheritance {
    private static String name;


    public PersonInheritance(String name) {
        PersonInheritance.name = name;
    }

    public static void sayHello(){
        System.out.println("Hello from " + name + "!");
    }

    public static void main(String[] args) {
        sayHello();
    }
}

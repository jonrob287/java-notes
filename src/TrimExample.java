import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TrimExample {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(1);
        list.add(1);
        list.add(1);
        //Iterator has to be of the same datatype of list
        Iterator<Integer> itr = list.iterator(); // calling list attaching iterator()
        System.out.println(list);
//        System.out.println(itr.next());// print out next iteration by one only
        while(itr.hasNext())// using this while we can get all the objects in the list
        {
            System.out.println(itr.next());// printing out array list using iterator
        }

    }
}

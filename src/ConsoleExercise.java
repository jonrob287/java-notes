import java.util.Scanner;

public class ConsoleExercise {
    public static void main(String[] args) {
        double pi = 3.14159;
//        System.out.println("The value or pi is approximately " + pi);
//        System.out.format("The value of pi is approximately %.2f", pi);
//        System.out.format("%s", pi);

        Scanner scanner = new Scanner(System.in);
//        System.out.print("Please enter a number: ");
//        int userInput = scanner.nextInt();
//        System.out.println(userInput);
//        What happens if you input something that is not an integer? error input miss match exception
//
//        System.out.print("Please enter three words: ");
//        String word1 = scanner.next();
//        String word2 = scanner.next();
//        String word3 = scanner.next();
//        System.out.println(word1);
//        System.out.println(word2);
//        System.out.println(word3);
//        What happens if you enter less than 3 words? you get stuck until you enter 3 words
//        What happens if you enter more than 3 words? you only get back the first three words


//        Prompt a user to enter a sentence, then store that sentence in a String variable using the .next method, then display that sentence back to the user.
//        do you capture all of the words? NO
//        Rewrite the above example using the .nextLine method. Now we get the whole sentence

//        System.out.print("Enter a sentence: ");
//        String userInput = scanner.nextLine();
//        System.out.println(userInput);

//        Calculate the perimeter and area of Code Bound's classrooms
//        Prompt the user to enter values of length and width of a classroom at Code Bound.
//        Use the .nextLine method to get user input and cast the resulting string to a numeric type.
//      •	Assume that the rooms are perfect rectangles.
//      •	Assume that the user will enter valid numeric data for length and width.
//        Display the area and perimeter of that classroom.
//        The area of a rectangle is equal to the length times the width, and the perimeter of a rectangle is equal to 2 times the length plus 2 times the width.

//        System.out.print("What is the length and width in feet of the classroom?");
//        int userInputLength = Integer.parseInt(scanner.next());
//        int userInputWidth = Integer.parseInt(scanner.next());
//        System.out.println("The area of the classroom is " + userInputLength * userInputWidth + " feet.");
//        System.out.println("The perimeter of the classroom is " + (userInputLength*2 + userInputWidth*2) + " feet.");

//                Bonuses
//          •	Accept decimal entries
//          •	Calculate the volume of the rooms in addition to the area and perimeter
//        System.out.print("What is the length, width, and height in feet of the classroom?");
//        double userInputLength = Double.parseDouble(scanner.next());
//        double userInputWidth = Double.parseDouble(scanner.next());
//        double userInputHeight = Double.parseDouble(scanner.next());
//        System.out.println("The area of the classroom is " + userInputLength * userInputWidth + " feet.");
//        System.out.println("The perimeter of the classroom is " + (userInputLength*2 + userInputWidth*2) + " feet.");
//        System.out.println("The volume of the classroom is " + userInputHeight * userInputLength *userInputWidth + " feet.");



//        Stephens code
        System.out.println("What is the length, width, and height in feet of the classroom?");
        String length = scanner.nextLine();
        int l = Integer.parseInt(length);

        String width = scanner.nextLine();
        int w = Integer.parseInt(width);

        System.out.println("The perimeter of the classroom is " + (l * 2 + w * 2));

        System.out.println("The area of the classroom is " + (l*w));

    }
}


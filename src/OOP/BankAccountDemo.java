package OOP;

public class BankAccountDemo {
    public static void main(String[] args) {
        //creating an account
        BankAccount a = new BankAccount();
        a.setBalance(19.00);

        //creating another account
        BankAccount b = new BankAccount();
        b.setBalance(400.50);

        System.out.println(a.getBalance());
        System.out.println(b.getBalance());


        BankAccount.setInterestRate(3.75);
//        System.out.println("First: $" + a.getInterestRate());
//        System.out.println("Second: $" + b.getInterestRate());

        System.out.println("Interest rate is " + BankAccount.getInterestRate() + "%");
        System.out.println("Interest earned is $" + ((BankAccount.getInterestRate()/100)*a.getBalance()));
        System.out.println("Interest earned is $" + ((BankAccount.getInterestRate()/100)*b.getBalance()));
        System.out.println("Total in your account after interest is $" + ((BankAccount.getInterestRate()/100)*a.getBalance()+a.getBalance()));
        System.out.println("Total in your account after interest is $" + ((BankAccount.getInterestRate()/100)*b.getBalance()+b.getBalance()));
    }


}

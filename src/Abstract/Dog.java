package Abstract;

public class Dog extends Animal{
    @Override
    public void eat() {
        System.out.println("I am a dog");
    }
}

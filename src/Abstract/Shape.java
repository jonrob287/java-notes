package Abstract;

public abstract class Shape {
    final int b = 20;
    public void display(){
        System.out.println("This is my display method..");
    }
    abstract public double calculateArea(double a, double b);
}
class Rectangle extends Shape{
    public static void main(String[] args) {
        Rectangle obj = new Rectangle();
        obj.display();
        System.out.println(obj.b);
        System.out.println(obj.calculateArea(22,12));

    }
    @Override
    public double calculateArea(double a, double b) {
        return a * b;
    }
}

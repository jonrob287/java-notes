import java.util.Scanner;

public class MethodsExercise {
    public static void main(String[] args) {
//        System.out.println(addMethod(5, 5));
//        System.out.println(subMethod(40,20));
//        System.out.println(multMethod(5, 10));
//        System.out.println(divMethod(100,4));
//        System.out.println(modulusMethod(3,99));
getInteger(0,10);
    }//End of Main

    //    public static int addMethod(int x,int y){
//        return x + y;
//    }
//    public static int subMethod(int x, int y){
//        return x - y;
//    }
//    public static int multMethod(int x, int y){
//        return x * y;
//    }
//    public static int divMethod(int x, int y){
//        return x/y;
//    }
//    public static int modulusMethod(int x, int y){
//        return x%y;
//    }
    public static int getInteger(int min, int max) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number between 1 and 10: ");
        int input = scanner.nextInt();
        if (input>= min && input <= max){
            System.out.println("Thank you for following instructions!");
            return input;
        }else{
            System.out.println("Invalid entry");
            return getInteger(min, max);
        }
    }


} //End of Class

/*
        Create a method that validates that user input is in a certain range

        The method signature should look like this:
        public static int getInteger(int min, int max);
        and is used like this:
        System.out.print("Enter a number between 1 and 10: ");
        int userInput = getInteger(1, 10);
        If the input is invalid, prompt the user again.
        Hint: recursion might be helpful here!





  Bonus
        Create your multiplication method without the * operator (Hint: a loop might be helpful).
        Do the above with recursion.
 */

package Constructors;
//creating class Shirt
public class Shirt {
    //creating instance variables
    public String color;
    public char size;

    //CUSTOM CONSTRUCTOR
    //    public Shirt(String color, char size) {
    //    this.color = color;
    //    this.size = size;
    //    }

    //creat method putOn() and takeOff()
    public static void putOn(){
        System.out.println("Shirt is on!");
    }
    public static void takeOff(){
        System.out.println("Shirt is off!");
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public char getSize() {
        return size;
    }

    public void setSize(char size) {
        this.size = size;
    }
}

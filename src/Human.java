import Constructors.Shirt;

public class Human {
    public String firstName;
    public String lastName;

    public Human(String firstName, String lastName) {
//        Human firstHuman = new Human();
        this.firstName = firstName;
        this.lastName = lastName;

        Human a = new Human("Jon", "Rob");
        a.setFirstName("Jon");
        a.setLastName("Rob");
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public static void main(String[] args) {

//        System.out.println(a.firstName);
//        System.out.println(a.lastName);
    }

}


/*
public String getName(){
//TODO: return the person's name
}
public void setName(String name){
//TODO: change the name property to the passed value
}
public void sayHello(){
//TODO: print a message to the console using the person's name
}
The class should have a constructor that accepts a `String` value and sets
the person's name to the passed string.
Create a `main` method on the class that creates a new `Person` object and
tests the above methods.
 */

public class ParentClass {
    //parent class constructor below
    public ParentClass() {
        System.out.println("Constructor of parent class");
    }
    void display(){
        System.out.println("I am a parent method named display");
    }
}//end of parentClass

class JavaExample extends  ParentClass {
    JavaExample() {
        //by default this JavaExample constructor invokes the constructor of the parent class
        //You can use super() to call the constructor of parent
        //It should be the first statement in the child class
        //constructor can also call the parameterized constructor of the parent class by using super like this:
        //super(10), now this will invoke the parameterized of int arg.
        System.out.println("Constructor of the child.");
    }
    void display(){
        System.out.println("Child method who overrides parent's method");

        super.display();

    }
    public static void main(String[] args) {

        JavaExample obj = new JavaExample();
        obj.display();
    }
}

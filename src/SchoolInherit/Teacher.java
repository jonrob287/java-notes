package SchoolInherit;
public class Teacher {
    String designation = "Teacher";
    String collegeName = "beginnersBook";
    void does() {  //added a method does()
        System.out.println("Teaching");  //returns teaching
    }
}//end of main class
class PhysicsTeacher extends Teacher {
    String mainSubject = "Physics";
}

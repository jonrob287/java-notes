package Inherit1;

public class Animal {
    protected String type;

    private String color;

    private int age;

    public void eat(){
        System.out.println("I am eating!");
    }
    public void sleep(){
        System.out.println("i am sleeping!");
    }

    public String getColor() {
        return color;
    }

    public void setColor(String col) {
        color = col;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.eat();
    }
}
class Dog extends Animal{

    @Override
    public void eat() {
//        System.out.println("I am eating dog food");
        super.eat();
    }


    public void displayInfo(String c, String t, int a){
        System.out.println("I am a " + t);

        System.out.println("My color is " + c);

        System.out.println("I am " + a);
    }
    String breed;
    public void bark(){ }
}
class Cat extends Animal{
    int age;
    public void meow(){ }
}

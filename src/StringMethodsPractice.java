import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.Arrays;
import java.util.Scanner;

public class StringMethodsPractice {
    public static void main(String[] args) {
//        String myLength = "Good afternoon, good evening, and good night";
//        System.out.println(myLength.length());
//        System.out.println(myLength.toUpperCase());
//        System.out.println(myLength.toLowerCase());

//        String firstSubstring = "Hello World".substring(6);
//        System.out.println(firstSubstring); // skips the first 6 characters of the string
//        String firstSubstring = "Hello World".substring(3);
//        System.out.println(firstSubstring); // skips the first 3 characters of the string

//        String firstSubstring = "Hello World".substring(10);
//        System.out.println(firstSubstring); // skips the first 10 characters of the string

//        String message = "Good evening, how are you?".substring(14); // prints our "how are you?"
//        System.out.println(message);
//        String message = "Good evening, how are you?".substring(0,12); //supposed to print out good evening // thank you adrian for showing that you can use two parameters
//        System.out.println(message);

//        String myChar = "San Antonio";
//        var res = myChar.charAt(0); //returns S
//        System.out.println(res);
//        var res = myChar.charAt(4); //returns A
//        System.out.println(res);
//        var res = myChar.charAt(7); // returns first o
//        System.out.println(res);

//        String bravo = "Jonathan Adrian Henry Eric MaryAnn Sandra";
//        String[] splitBravo = bravo.split(" ");             // do not put comma as regex
//        System.out.println(Arrays.toString(splitBravo));
//
//        String m1 = "Hello, ";
//        String m2 = "how are you?";
//        String m3 = "I love Java!";
//        System.out.println(m1 + m2 + " " + m3);
//
//        int result = 89;
//        System.out.println("You scored " + result + " marks for your test!");

//        String m1 = "I never wanna hear you say";
//        String m2 = "I want it that way!";
//        String message = m1 + "\n"+ m2;
//        System.out.println(message);

//        String message = "Check \"this\" out!, \"s inside of \"s!\"";
//        System.out.println(message);

//        String m1 = "In windows, the main drive is usually C:\\";
//        String m2 = "I can do backslashes \\, double backslashes \\\\,";
//        String m3 = "and the amazing triple backslash \\\\\\!";
//        System.out.println(m1 + "\n" + m2 + "\n" + m3);

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter date in MM/DD/YYYY format that you would like to be converted to MonthName DD, YYYY format: ");
        int userInputMonth = sc.nextInt();
        int userInputDay = sc.nextInt();
        int userInputYear =  sc.nextInt();
        if(userInputMonth == 1){
            System.out.println("January " + userInputDay + ", " + userInputYear);
        }else{
            System.out.println("You didn't enter a proper date");
        }
    }
}
/*
BONUS
Create date format converter application.
Take in the following format:
MM/DD/YYYY
Output the following:
MonthName DD, YYYY
Example:
user input - 12/01/1999
console output - December 1, 1999
 */

import java.util.Scanner;

//public class ControlFlowExercises {
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        String choice = "y";
//        do {
//            System.out.println("Please enter grade from 0 - 100: ");
//            int userInput = sc.nextInt();
//            if (userInput > 100) {
//                System.out.println("You entered a number above 100.");
//            }else if (userInput >= 90){
//                System.out.println("A");
//            }else if (userInput >=80) {
//                System.out.println("B");
//            }else if (userInput >= 70) {
//                System.out.println("C");
//            } else if (userInput >= 60) {
//                System.out.println("D");
//            }else if (userInput >= 0) {
//                System.out.println("F");
//            }else {
//                System.out.println("You did not enter a correct number.");
//            }
//            System.out.print("Continue? (y/n): ");
//            choice = sc.next();
//            System.out.println();
//        } while (!choice.equalsIgnoreCase("n"));
//    }
//}
/*
BONUS ATTEMPT
 */
public class ControlFlowExercises {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String choice = "y";
        do {
            System.out.println("Please enter grade from 0 - 100: ");
            int userInput = sc.nextInt();
            if (userInput > 100) {
                System.out.println("You entered a number above 100.");
            }else if (userInput >= 98){
                System.out.println("A+");
            }else if (userInput >=94) {
                System.out.println("A");
            }else if (userInput >= 90) {
                System.out.println("A-");
            } else if (userInput >= 87) {
                System.out.println("B+");
            }else if (userInput >= 83) {
                System.out.println("B");
            }else if (userInput >= 80) {
                System.out.println("B-");
            }else if (userInput >= 77) {
                System.out.println("C+");
            }else if (userInput >= 73) {
                System.out.println("C");
            }else if (userInput >= 70) {
                System.out.println("C-");
            }else if (userInput >= 67) {
                System.out.println("D+");
            }else if (userInput >= 63) {
                System.out.println("D");
            }else if (userInput >= 60) {
                System.out.println("D-");
            }else if (userInput >= 0) {
                System.out.println("F");
            }else {
                System.out.println("You did not enter a correct number.");
            }
            System.out.print("Continue? (y/n): ");
            choice = sc.next();
            System.out.println();
        } while (!choice.equalsIgnoreCase("n"));
    }
}
/*
         Convert given number grades into letter grades.
        Prompt the user for a numerical grade from 0 to 100.
        Display the corresponding letter grade.
        Prompt the user to continue.
        Assume that the user will enter valid integers for the grades.
        The application should only continue if the user agrees to.
        Grade Ranges:
        A : 100 - 90
        B : 89 - 80
        C : 79 - 70
        D : 66 - 60
        F : 59 - 0
        Bonus
        Edit your grade ranges to include pluses and minuses (ex: 98-100 = A+, 94-97 = A, 90-93 = A-).
 */

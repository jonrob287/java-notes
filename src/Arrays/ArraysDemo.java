package Arrays;
import java.util.Arrays;

public class ArraysDemo {
    //Arrays
    //List of data that contains zero or more items called elements
    //Arrays can only be of one data-type within a single array

    //Syntax for array
    double [] prices;
    int [] integers;
    String [] names;
    float [] floatingNumbers;

    public static void main(String[] args) {
        String [] names = new String[6];
        names[0] = "Jonathan";
        names[1] = "Adrian";
        names[2] = "Sandra";
        names[3 ]= "MaryAnn";
        names[4] = "Henry";
        names[5] = "Eric";
//        names[6] = "Instructors";
        System.out.println(names[3]);
        String [] avengers = {"Iron man", "Hulk", "Captain America", "Thor", "Black Widow", "HawkEye"};
        System.out.println(avengers.length);
        System.out.println(avengers[5]);


        //iterating over arrays
        int [] numbers = new int[5];
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = 3;
        numbers[3] = 4;
        numbers[4] = 5;

        for(int i = 0; i < numbers.length; i++){
            System.out.println(numbers[i]);
        }

        String[] languages = {"html", "css", "javascript", "angular", "java"};
        for(int i = 0; i < languages.length; i++){
        System.out.println(languages[i]);
        }

        //enhanced for loop with same result of printing the array
        for(String language : languages){
            System.out.println(language);
        }
        // Example iterating through 2d array
        int [] [] matrix = {
                {1,2,3},
                {4,5,6},
                {7,8,9}
        };
        for(int i = 0; i < matrix.length; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }
    }
}

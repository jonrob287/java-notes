package Arrays;

public class multidimensionalArray {
    public static void main(String[] args) {
        //create a 2d array
        int[][] a = {
                {1, 2, 3, 4, 5},
                {6, 7, 8, 9},
                {7}
        };

        //calculate length of each row
        System.out.println("Length of row 1 is " + a[0].length);
        System.out.println("First element first row " + a[0][0]);
        System.out.println("Third element in second row " + a[1][2]);
        System.out.println("Third element in second row " + a[1][3]);
        System.out.println("Third element in second row " + a[2][0]);


    }
}

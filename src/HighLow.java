import java.util.Random;
import java.util.Scanner;

public class HighLow {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random rand = new Random();
        int randomNum = rand.nextInt(100);
        int correctNum = randomNum + 1;
        while (true) {
            System.out.println("enter a number 0-100");
            int guess1 = input.nextInt();
            if(guess1 < correctNum){
                System.out.println("number is too low!");
            }
            else if(guess1 > correctNum){
                System.out.println("Number is too high!");
            }
            else {
                System.out.println("correct!");
                break;
            }
        }
    }// END OF MAIN

}// END OF CLASS

